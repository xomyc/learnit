package com.it.learn.ui.book;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.it.learn.R;
import com.it.learn.domain.local.Word;
import com.it.learn.ui.base.BasePresenter;
import com.it.learn.util.NoMoreElementsException;
import com.it.learn.util.StorageService;
import com.it.learn.util.parser.Parser;
import com.it.learn.util.parser.ParserFactory;

import java.util.ArrayList;

import io.reactivex.disposables.Disposable;

public class BookPresenter extends BasePresenter<BookMvpView> {
//    TODO: should be calculated on runtime instead
    private static final int PER_PAGE = 9; //9 items can be shown in bookFragment's recycler without scroll

    private Parser parser;
    private StorageService storageService;
    private ArrayList<Word> words = new ArrayList<>();

    @Override
    public void attachView(@NonNull BookMvpView mvpView, Bundle savedInstanceState) {
        super.attachView(mvpView, savedInstanceState);

        Context context = mvpView.getContext();

//        TODO: DI needed
        storageService = StorageService.getInstance(context);

//        TODO: parser should be preserved, to avoid repeated parses
        parser = ParserFactory.create(R.raw.book, mvpView.getContext());
        showNextPage();
    }

    public void onNextPressed() {
        for (Word currentItem : words) {
            if (currentItem.isKnown == null) {
                currentItem.isKnown = false;
            }
        }
        updateStorage(words);
        showNextPage();
    }

    public void onKnowAllPressed() {
        for (Word currentItem : words) {
            currentItem.isKnown = true;
        }
        updateStorage(words);
        showNextPage();
    }

    private void showNextPage(){
        getMvpView().setLoading(true);
        Disposable getWordsDisposable = parser
                .getWords(PER_PAGE)
                .subscribe(
                        words -> {
                            this.words = new ArrayList<>(words);
                            getMvpView().showItems(this.words);
                            getMvpView().setLoading(false);
                        },

                        throwable -> {
                            if (throwable instanceof NoMoreElementsException) {
                                onBookFinished();
                            } else {
                                throwable.printStackTrace();
                            }
                            getMvpView().setLoading(false);
                        }
                );

        pendingRequestsDisposable.add(getWordsDisposable);
    }

    private void updateStorage(ArrayList<Word> words) {
        if (words == null ||  words.isEmpty()) {
            return;
        }

        storageService.addWords(words);
    }

    private void onBookFinished() {
        getMvpView().finish();
    }

    public void onItemCheckedChanged(int position, boolean isChecked) {
        Word word = words.get(position);
        if (word != null) {
            word.isKnown = isChecked;
        }

        if (isAllWordsChecked()) {
            updateStorage(words);
            showNextPage();
        }
    }

    private boolean isAllWordsChecked() {
        boolean isAllItemsChecked = true;
        for (Word word : words) {
            if (word.isKnown == null){
                isAllItemsChecked = false;
                break;
            }
        }

        return isAllItemsChecked;
    }


}

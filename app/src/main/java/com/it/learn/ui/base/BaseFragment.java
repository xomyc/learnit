package com.it.learn.ui.base;

import androidx.fragment.app.Fragment;

public abstract class BaseFragment<T extends BasePresenter> extends Fragment {

    protected T presenter;

    @Override
    public void onDestroyView() {
        if (presenter != null) {
            presenter.detachView();
        }

        super.onDestroyView();
    }
}

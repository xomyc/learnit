package com.it.learn.ui.base;

import android.os.Bundle;

import io.reactivex.disposables.CompositeDisposable;

public class BasePresenter<T extends BaseMvpView> {

    private T mvpView;
    protected CompositeDisposable pendingRequestsDisposable = new CompositeDisposable();

    public void attachView(T mvpView, Bundle savedInstanceState) {
        this.mvpView = mvpView;
    }

    public void detachView() {
        if (pendingRequestsDisposable != null) {
            pendingRequestsDisposable.clear();
        }
        mvpView = null;
    }

    public T getMvpView() {
        return mvpView;
    }

}

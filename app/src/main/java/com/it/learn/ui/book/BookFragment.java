package com.it.learn.ui.book;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.it.learn.R;
import com.it.learn.domain.local.Word;
import com.it.learn.ui.base.BaseFragment;

import java.util.ArrayList;

public class BookFragment extends BaseFragment<BookPresenter> implements BookMvpView {

    private BookStructureAdapter bookStructureAdapter;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new BookPresenter();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.book_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.words_recycler);
        progressBar = view.findViewById(R.id.words_progress);

        view.findViewById(R.id.words_mark_all_known).setOnClickListener(v -> presenter.onKnowAllPressed());
        view.findViewById(R.id.words_mark_all_learn).setOnClickListener(v -> presenter.onNextPressed());

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
                bookStructureAdapter = new BookStructureAdapter();
        recyclerView.setAdapter(bookStructureAdapter);

        presenter.attachView(this, savedInstanceState);
    }

    @Override
    public void finish() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.getSupportFragmentManager().popBackStackImmediate();
        }
    }

    @Override
    public void showItems(ArrayList<Word> items) {
        bookStructureAdapter.items = items;
        bookStructureAdapter.notifyDataSetChanged();
    }

    @Override
    public void setLoading(boolean isLoading) {
        recyclerView.setVisibility(isLoading ? View.INVISIBLE : View.VISIBLE);
        progressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    private class BookStructureAdapter extends RecyclerView.Adapter<WordViewHolder> implements CompoundButton.OnCheckedChangeListener {

        private ArrayList<Word> items = new ArrayList<>();

        @NonNull
        @Override
        public WordViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_list_item, parent, false);
            return new WordViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull WordViewHolder holder, int position) {
            Word word = items.get(position);
            if (word == null) {
                return;
            }
            holder.knowSwitch.setText(word.lemma);
            holder.knowSwitch.setTag(R.id.tag_holder_adapter_position, position);

            //"disabling" checked change listeners
            holder.knowSwitch.setOnCheckedChangeListener(null);
            holder.knowSwitch.setChecked(word.isKnown != null && word.isKnown);
            holder.knowSwitch.setOnCheckedChangeListener(this);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            Object tag = buttonView.getTag(R.id.tag_holder_adapter_position);
            if (tag instanceof Integer) {
                int position = (Integer) tag;
                presenter.onItemCheckedChanged(position, isChecked);
            }
        }

    }

    private class WordViewHolder extends RecyclerView.ViewHolder {
        public SwitchCompat knowSwitch;

        public WordViewHolder(@NonNull View itemView) {
            super(itemView);
            knowSwitch = (SwitchCompat) itemView;
        }
    }




}

package com.it.learn.ui.activity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.it.learn.R;
import com.it.learn.ui.book.BookFragment;
import com.it.learn.util.StorageService;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

//TODO: doesn't implement mvp, since it's not supposed to handle any kind of business logic, instead it should just navigate between fragments
public class MainActivity extends AppCompatActivity {

    private CompositeDisposable pendingRequestsDisposable = new CompositeDisposable();

    private TextView wordsKnown;
    private TextView wordsToLearn;

    private StorageService storageService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        TODO: DI needed
        storageService = StorageService.getInstance(this);

        setContentView(R.layout.main_activity);

        wordsKnown = findViewById(R.id.main_words_known);
        wordsToLearn = findViewById(R.id.main_words_to_learn);

        findViewById(R.id.main_load_words).setOnClickListener(v -> onLoadWordsPressed());

        updateDataFromStorage();

//        TODO: should be changed to event at least
//        some kind of result handling
        getSupportFragmentManager().addOnBackStackChangedListener(this::updateDataFromStorage);
    }

    @Override
    protected void onDestroy() {
        pendingRequestsDisposable.clear();
        super.onDestroy();
    }

    private void onLoadWordsPressed() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.main_fragment_container, new BookFragment());
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }

    public void updateDataFromStorage() {
        Disposable requestWordsKnownDisposable = storageService
                .requestWordsKnown()
                .subscribe(
                        words -> {
                            int knownWordsCount = words.size();
                            wordsKnown.setText(getString(R.string.main_words_known, knownWordsCount));
                        },
                        Throwable::printStackTrace
                );

        pendingRequestsDisposable.add(requestWordsKnownDisposable);

        Disposable requestWordsToLearnDisposable = storageService
                .requestWordsToLearn()
                .subscribe(
                        words -> {
                            int wordsToLearnCount = words.size();
                            wordsToLearn.setText(getString(R.string.main_words_to_learn, wordsToLearnCount));
                        },
                        Throwable::printStackTrace
                );

        pendingRequestsDisposable.add(requestWordsToLearnDisposable);
    }
}

package com.it.learn.ui.base;

import android.content.Context;

public interface BaseMvpView {

    Context getContext();

    void finish();
}

package com.it.learn.ui.book;

import com.it.learn.domain.local.Word;
import com.it.learn.ui.base.BaseMvpView;

import java.util.ArrayList;

public interface BookMvpView extends BaseMvpView {
    void showItems(ArrayList<Word> currentItems);

    void setLoading(boolean isLoading);
}

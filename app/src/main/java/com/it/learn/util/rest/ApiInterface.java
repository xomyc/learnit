package com.it.learn.util.rest;

import com.it.learn.domain.remote.AnswerAnalyze;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("analyze")
    Single<AnswerAnalyze> analyzeText(@Query("data") String data, @Query("output") String output);
}

package com.it.learn.util.rest;

import android.content.Context;

import com.it.learn.R;
import com.it.learn.domain.remote.AnswerAnalyze;
import com.it.learn.util.SchedulersHelper;

import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class RestService {
    private ApiInterface apiInterface;

    public RestService(Context context) {
        String apiBaseUrl = context.getResources().getString(R.string.base_url);
        String apiEndpoint = apiBaseUrl + context.getResources().getString(R.string.api_enpoint_path);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(apiEndpoint)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create())
                .client(initHttpClient())
                .build();
        apiInterface = retrofit.create(ApiInterface.class);
    }

    private OkHttpClient initHttpClient(){
        OkHttpClient.Builder okHTTPBuilder = new OkHttpClient.Builder();
        okHTTPBuilder
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS);

        return okHTTPBuilder.build();
    }

    public Single<AnswerAnalyze> analyzeText(String text){
        return apiInterface
                .analyzeText(text, "json")
                .compose(SchedulersHelper.applyAsyncSchedulers());
    }


}

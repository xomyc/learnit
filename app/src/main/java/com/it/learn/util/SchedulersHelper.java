package com.it.learn.util;

import android.os.AsyncTask;

import io.reactivex.SingleTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SchedulersHelper {

    public static <T> SingleTransformer<T, T> applyAsyncSchedulers() {
        return upstream -> upstream
                .subscribeOn(Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR))
                .observeOn(AndroidSchedulers.mainThread());
    }
}

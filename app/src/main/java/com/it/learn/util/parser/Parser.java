package com.it.learn.util.parser;

import com.it.learn.domain.local.Word;

import java.util.ArrayList;

import io.reactivex.Single;

/**
 * Abstract representation of parser. Should be extended for any kind of parsing method or language
 */
public abstract class Parser {

    /**
     * returns next page of words
     *
     * @param perPage - amount of words on page
     */
    public abstract Single<ArrayList<Word>> getWords(int perPage);
}

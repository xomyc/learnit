package com.it.learn.util.parser;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.it.learn.domain.local.Book;
import com.it.learn.domain.local.BookSection;
import com.it.learn.domain.local.Word;
import com.it.learn.domain.remote.AnalysisResult;
import com.it.learn.domain.remote.AnalysisResultOption;
import com.it.learn.domain.remote.AnswerAnalyze;
import com.it.learn.util.NoMoreElementsException;
import com.it.learn.util.StorageService;
import com.it.learn.util.rest.RestService;

import java.util.ArrayList;
import java.util.HashSet;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Parser for czech language with <a href="http://ufal.mff.cuni.cz/morphodita">Morphodita</a>
 */
public class CzechMorphoditaParser extends Parser {
    private static final int REQUEST_TEXT_MIN_LENGTH = 300;

    private final Book book;
    private ArrayList<Word> words = new ArrayList<>();
    private int proceededWordsCount;
    private RestService restService;
    private StorageService storageService;
    private ArrayList<BookSection> sectionsInProgress = new ArrayList<>();

    public CzechMorphoditaParser(Book book, Context context) {
        this.book = book;
//        TODO: DI needed
        restService = new RestService(context);
        storageService = StorageService.getInstance(context);
    }

    @Override
    public Single<ArrayList<Word>> getWords(int perPage) {
        if (words.size() >= proceededWordsCount + perPage) {
            return requestWordsFromCollection(perPage);
        } else {
            return requestWordsFromApi(perPage);
        }
    }

    private Single<ArrayList<Word>> requestWordsFromCollection(int perPage){
        int maxElementPosition = Math.min(words.size(), proceededWordsCount + perPage); //to prevent IndexOutOfBound
        ArrayList<Word> words = new ArrayList<>(this.words.subList(proceededWordsCount, maxElementPosition));
        proceededWordsCount = maxElementPosition;
        return Single.just(words);
    }

    private Single<ArrayList<Word>> requestWordsFromApi(int perPage){
        String nextText = getNextText();
        if (TextUtils.isEmpty(nextText)) {
            return Single.error(new NoMoreElementsException());
        }

        return restService
                .analyzeText(nextText)
                .observeOn(Schedulers.computation())
                .map(this::mapToWords)
                .flatMap(
                        stringsSet -> {
                            for (BookSection sectionsInProgress : sectionsInProgress) {
                                sectionsInProgress.isParsed = true;
                            }
                            sectionsInProgress.clear();
                            for (String s : stringsSet) {
                                Word word = Word.create(s);
                                if (word == null || words.contains(word)) {
                                    continue;
                                }
                                words.add(word);
                            }
                            return requestWordsFromCollection(perPage);
                        }
                )
                .observeOn(AndroidSchedulers.mainThread());
    }

    private String getNextText(){
        StringBuilder textBuilder = new StringBuilder();
        do {
            BookSection nextSectionToProceed = book.getNextSectionToProceed();
            if (nextSectionToProceed != null) {
                sectionsInProgress.add(nextSectionToProceed);
                textBuilder.append(nextSectionToProceed.text);
                textBuilder.append(" "); //to prevent words merging at sections joint
            } else {
                break;
            }
        } while (textBuilder.length() < REQUEST_TEXT_MIN_LENGTH);

        return textBuilder.toString();
    }

    private HashSet<String> mapToWords (AnswerAnalyze answerAnalyze){
        HashSet<String> result = new HashSet<>();
        for (ArrayList<AnalysisResult> analysisResults : answerAnalyze.result) {
            for (AnalysisResult analysisResult : analysisResults) {
                if (analysisResult.analyses != null) {
                    for (AnalysisResultOption analysis : analysisResult.analyses) {
                        String[] split = analysis.lemma.split("[_\\-`]+",2); // -,_ and ` are comment prefixes for lemmas e.g. "jet-1_^"
                        if (split.length > 0) {
                            result.add(split[0]);
                            break;
                        }
                    }
                }
            }
        }
        return result;
    }

}

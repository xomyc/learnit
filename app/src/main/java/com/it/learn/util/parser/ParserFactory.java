package com.it.learn.util.parser;

import android.content.Context;

import androidx.annotation.RawRes;

import com.it.learn.domain.local.Book;
import com.it.learn.domain.local.FbBook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class ParserFactory {

    public static Parser create(Book book, Context context){
//        TODO: implement more parsers
        return new CzechMorphoditaParser(book, context);
    }

    public static Parser create(File file, Context context){
        return create(new FbBook(file), context);
    }

    public static Parser create(@RawRes int rawResources, Context context){
        File booksFolder = new File(context.getFilesDir(), "books");
        booksFolder.mkdirs();
        File file = new File(booksFolder, "raw_copy.fb2");
        copyRawRes(rawResources, file, context);
        return create(file, context);
    }

    private static void copyRawRes(@RawRes int rawResource, File destination, Context context){
        try (InputStream in = context.getResources().openRawResource(rawResource);
             OutputStream out = new FileOutputStream(destination)) {
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

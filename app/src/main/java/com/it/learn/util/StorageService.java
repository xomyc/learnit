package com.it.learn.util;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.it.learn.domain.local.Word;

import java.util.ArrayList;
import java.util.HashSet;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

//TODO: change SharedPreferences to database
public class StorageService {

    private static StorageService instance;

    private static final String PREFERENCES_NAME = "learnit.pref";
    private static final String PREFERENCES_KEY_WORDS = "preferences_key_words";

    private SharedPreferences sharedPreferences;
    private HashSet<Word> words;

//    TODO: DI needed
    public static synchronized StorageService getInstance(Context context){
        if (instance == null) {
            instance = new StorageService(context);
        }
        return instance;
    }

    private StorageService(Context context) {
        sharedPreferences = context.getSharedPreferences(PREFERENCES_NAME, Application.MODE_PRIVATE);
    }

    public Single<HashSet<Word>> requestWordsKnown() {
        return getWords()
                .observeOn(Schedulers.computation())
                .map(
                        words -> {
                            HashSet<Word> knownWords = new HashSet<>();
                            for (Word word : words) {
                                if (word.isKnown) {
                                    knownWords.add(word);
                                }
                            }
                            return knownWords;
                        }
                )
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<HashSet<Word>> requestWordsToLearn() {
        return getWords()
                .observeOn(Schedulers.computation())
                .map(
                        words -> {
                            HashSet<Word> wordsToLearn = new HashSet<>();
                            for (Word word : words) {
                                if (!word.isKnown) {
                                    wordsToLearn.add(word);
                                }
                            }
                            return wordsToLearn;
                        }
                )
                .observeOn(AndroidSchedulers.mainThread());
    }

    public void addWords(ArrayList<Word> newWords) {
        this.words.addAll(newWords);
        saveWords();
    }

    private void saveWords(){
        String seenPromoPostsString = new Gson().toJson(words);
//        TODO: possible concurrency
        sharedPreferences.edit().putString(PREFERENCES_KEY_WORDS, seenPromoPostsString).apply();
    }

    public Single<HashSet<Word>> getWords() {
        if (words != null) {
            return Single.just(words);
        } else {
            return loadWords()
                    .compose(SchedulersHelper.applyAsyncSchedulers());
        }
    }

    private Single<HashSet<Word>> loadWords() {
//        TODO: threads and sharing
        return Single
                .create(
                    emitter -> {
                        String wordsKnownJson = sharedPreferences.getString(PREFERENCES_KEY_WORDS, null);
                        if (!TextUtils.isEmpty(wordsKnownJson)) {
                            words = new Gson().fromJson(wordsKnownJson,  new TypeToken<HashSet<Word>>(){}.getType());
                        } else {
                            words = new HashSet<>();
                        }
                        emitter.onSuccess(words);
                    }

        );
    }


}

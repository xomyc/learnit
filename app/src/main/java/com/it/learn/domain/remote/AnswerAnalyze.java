package com.it.learn.domain.remote;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AnswerAnalyze {
    public String model;
    public ArrayList<ArrayList<AnalysisResult>> result;
}

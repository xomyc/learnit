package com.it.learn.domain.local;

import android.text.TextUtils;

import com.kursx.parser.fb2.Element;
import com.kursx.parser.fb2.FictionBook;
import com.kursx.parser.fb2.Section;

import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.xml.parsers.ParserConfigurationException;

/**
 * Fiction Book implementation of {@link Book}
 */
public class FbBook extends Book {

    private FictionBook fictionBook;
    private int lastRequestedSectionPosition;

    public FbBook(File file) {

        try {
            fictionBook = new FictionBook(file);
        } catch (ParserConfigurationException | IOException | SAXException e) {
//          TODO: handle this
            e.printStackTrace();
        }

        if (fictionBook == null) {
            return;
        }

        sections = new ArrayList<>();
        ArrayList<Section> bookSections = fictionBook.getBody().getSections();
//        TODO: possible StackOverflowException
        for (Section section : bookSections) {
            sections.addAll(extractSections(section));
        }

        sections = Collections.unmodifiableList(sections);
    }

    /**
     * Flattens {@link FictionBook} sections hierarchy to one-level list of {@link BookSection}
     */
    private ArrayList<BookSection> extractSections(Section section){
        ArrayList<BookSection> result = new ArrayList<>();
        ArrayList<Section> innerSections = section.getSections();
        if (innerSections != null) {
            for (Section innerSection : innerSections) {
                result.addAll(extractSections(innerSection));
            }
        }

        ArrayList<Element> elements = section.getElements();
        if (elements != null) {
            for (Element element : elements) {
                String text = element.getText();
                if (!TextUtils.isEmpty(text)) {
                    BookSection bookSection = new BookSection(text);
                    result.add(bookSection);
                }
            }
        }
        return result;
    }
    
    @Override
    public BookSection getNextSectionToProceed() {
        if (sections == null || lastRequestedSectionPosition >= sections.size()) {
            return null;
        }

        return sections.get(++lastRequestedSectionPosition);
    }
}

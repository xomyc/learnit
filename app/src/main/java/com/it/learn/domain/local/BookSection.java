package com.it.learn.domain.local;

import androidx.annotation.NonNull;

/**
 * A part of book with text, which can be parsed to words
 */
public class BookSection {

    /**
     * Indicates if section was parsed to words already */
    public boolean isParsed;
    public String text;

    public BookSection(@NonNull String text) {
        this.text = text;
    }
}

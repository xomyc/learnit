package com.it.learn.domain.remote;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AnalysisResult {
    public String token;
    public ArrayList<AnalysisResultOption> analyses;
}

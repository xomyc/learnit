package com.it.learn.domain.local;

import java.util.List;

/**
 * Abstract representation of book. Should be extended for any kind of source, which can be
 * represented as book (fb2, epub, plain text, etc.)
 */
public abstract class Book {

    public List<BookSection> sections;

    /**
     * returns next section to parse to words
     */
    public abstract BookSection getNextSectionToProceed();
}

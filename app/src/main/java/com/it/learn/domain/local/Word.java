package com.it.learn.domain.local;

import android.text.TextUtils;

import androidx.annotation.NonNull;

public class Word {

    /**
     * the "base" form of word*/
    public final String lemma;

    /**
     * indicates if user has learned this word already */
    public Boolean isKnown;


    public static Word create(@NonNull String lemma) {
        if (TextUtils.isEmpty(lemma) || !lemma.matches("\\p{L}+")) { //empty or not a word
            return null;
        }

        return new Word(lemma);
    }


    private Word(@NonNull String lemma) {
        this.lemma = lemma;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word = (Word) o;
        return TextUtils.equals(lemma, word.lemma);
    }

    @Override
    public int hashCode() {
        return lemma.hashCode();
    }


}

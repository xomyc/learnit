package com.it.learn.domain.remote;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AnalysisResultOption {
    public String lemma;
    public String tag;
}
